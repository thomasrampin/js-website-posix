"use strict";

var count = 0; //init du compteur de point

$(document).ready(function() {
	
	$("#intro a").click(function(){
		$("#intro").fadeOut(800);
		$("#game").fadeIn(800);
		$("html, body").animate({scrollTop: "0"},800);
	});

	//Animation de barre de navigation au click
	$("#navbar li").mousedown(function() {
		$("#navbar li").removeClass("active");
		$(this).addClass("active");
	});
	
	$("#img-back").css("height", ($(window).height()));
        $("#img-back").on('dragstart', function(e){ e.preventDefault(); });

	//scroll pour le click dans le nav
	$("#rules-link").click(function(){
		$("html, body").animate({scrollTop:$("#rules").offset().top - 70}, "slow"); //70 pour le navbar
	});

	$("#about-link").click(function(){
		$("html, body").animate({scrollTop:$("#about").offset().top - 70}, "slow"); //70 pour le navbar
	});
	
	$("#contact-link").click(function(){
		$("html, body").animate({scrollTop:$("#contact").offset().top - 70}, "slow"); //70 pour le navbar
	});
	
	$("#games-link").click(function(event) {
		event.preventDefault();
		$("html, body").animate({scrollTop:$("html, body").offset().top}, "slow");
	});

	$("#brand-nav").click(function(event) {
		event.preventDefault();
		$("html, body").animate({scrollTop:$("html, body").offset().top}, "slow");
	});

	$(window).scroll(function() {
		setNavPosScroll();
	});

	dragMain();

});

//Fixer la navbar en haut lors du scroll
function setNavPosScroll() {
	var scrollPos = ($(window).scrollTop() );
	var windowHauteur = ($(window).height() ) - 50; //50 pour la taille de ma navbar
	if(scrollPos >= windowHauteur) {
		$("#mainNav").removeClass("navbar-fixed-bottom");
		$("#mainNav").css("position","fixed");
		$("#mainNav").css("width","100%");
	}
	if(scrollPos <= windowHauteur) {
		$("#mainNav").addClass("navbar-fixed-bottom");
		$("html, body").css("padding-bottom","60px");
	}
};

function dragMain(){
	$("#moustache-papi").draggable({
		start: function(){
			count++ ;
			console.log(count);
			if(count == 5){
				dragItaly();
			}
			if(count == 10){
				dragBiker();
			}
			if(count == 15){
				dragLot();
			}
			if(count == 30){
				dragGold();
			}
		}
	});
};


function dragItaly(){
	$("#msg-felice").fadeIn(1500).delay(5000).fadeOut(1500);
	$("#moustache-italy").fadeIn(300);
	$("#moustache-italy").draggable({
		start: function() {
			count ++;
                        if(count == 10){
                            dragBiker();
                        }
                        if(count == 15){
                            dragLot();
                        }
                        if(count == 30){
                            dragGold();
                        }
		}
	});
};

function dragBiker(){
	$("#msg-felice").fadeIn(1500).delay(5000).fadeOut(1500);
	$("#moustache-biker").fadeIn(300);
	$("#moustache-biker").draggable({
		start: function() {
			count ++;
                         if(count == 15){
                            dragLot();
                        }
                        if(count == 30){
                            dragGold();
                        }

		}
	});
};


function dragLot(){
	$("#msg-felice").fadeIn(1500).delay(5000).fadeOut(1500);
	$("#moustache-chaplin").fadeIn(300);
	$("#moustache-hipster").fadeIn(300);
	$("#moustache-bonhomme").fadeIn(300);
	$("#moustache-magicien").fadeIn(300);
	$("#moustache-chinoise").fadeIn(300);
	$("#moustache-chaplin").draggable({
		start: function() {
			count ++;
                        if(count == 30){
                            dragGold();
                        }

		}
	});

	$("#moustache-hipster").draggable({
		start: function() {
			count ++;
                        if(count == 30){
                            dragGold();
                        }

		}
	});

	$("#moustache-bonhomme").draggable({
		start: function() {
			count ++;
                        if(count == 30){
                            dragGold();
                        }

		}
	});

	$("#moustache-magicien").draggable({
		start: function() {
			count ++;
                        if(count == 30){
                            dragGold();
                        }

		}
	});

	$("#moustache-chinoise").draggable({
		start: function() {
			count ++;
                        if(count == 30){
                            dragGold();
                        }

		}
	});
};

function dragGold(){
	$("#msg-felice-golden").fadeIn(1500).delay(5000).fadeOut(1500);
	$("#moustache-golden").fadeIn(300);
	$("#moustache-golden").draggable();
};
/*function chargeImg(){
$.ajax({
	url : 

*/

/*function setContent(){
	$(
*/

